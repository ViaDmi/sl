# sent version
VERSION = 1

# auto parallel compilation
MAKEFLAGS+="-j -l $(shell grep -c ^processor /proc/cpuinfo)"

# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

# includes and libs
INCS = -I. -I/usr/include -I/usr/include/freetype2 -I${X11INC}
LIBS = -L/usr/lib -lc -lm -L${X11LIB} -lXft -lfontconfig -lX11

# flags
CPPFLAGS = -DVERSION=\"${VERSION}\" -D_XOPEN_SOURCE=600
CFLAGS += -std=c99 -pedantic -Wall -Os ${INCS} ${CPPFLAGS} -flto
LDFLAGS += ${LIBS}

# compiler and linker
CC ?= cc
