# sent
Simple plaintext presentation tool.

## Info about this custom build
### Patches
| Patch                                                                       | Short description                       |
| --------------------------------------------------------------------------- | --------------------------------------- |
| [progress bar](https://tools.suckless.org/sent/patches/progress-bar/)       | adds progress bar on the slide's bottom |
| [cmdline options](https://tools.suckless.org/sent/patches/cmdline_options/) | adds extra command line options         |
| [inverted colors](https://tools.suckless.org/sent/patches/inverted-colors/) | adds another color scheme               |
| [toggle colorscheme](https://tools.suckless.org/sent/patches/toggle-scm)    | toggle colorschemes during presentation |

### Additional editing
Not every patch provides updating `usage()` function and documentation, so I manually edit it.
Also I add setting progress bar height via command line argument `[-p height]`.

## Optional dependencies
You need Xlib and Xft to build sent and the [farbfeld](https://tools.suckless.org/farbfeld/) tools to use images in the presentations.
For example, [farbfeld in AUR](https://aur.archlinux.org/packages/farbfeld):
```console
~$ yay -S farbfeld
```
Maybe I should add this tool in this repo as subtree...
