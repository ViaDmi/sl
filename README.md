# Custom sl build
Custom build of some [suckless](https://suckless.org/) software
and some another software with same philosophy (for example see [rocks](https://suckless.org/rocks/)).
When you want Gentoo but compilation time scared you. By the way I use Artix.

| Program                                    | Short description         |
| ------------------------------------------ | ------------------------- |
| [dmenu](https://tools.suckless.org/dmenu/) | a dynamic menu for X      |
| [nsxiv](https://codeberg.org/nsxiv/nsxiv)  | new simple X image viewer |
| [sent](https://tools.suckless.org/sent/)   | simple presentation tool  |

## How this repo works
For patching and configuration [suckless website suggest](https://dwm.suckless.org/customisation/patches_in_git/)
to clone original repo and maintain own branch with custom modifications and merge with master/main for updating main code base.
I don't like this way because I don't want to maintain many small repositories.
The one of way to solve this issue is using [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
However, I think it is not the best way. Submodules usually needs when you need one project in other as dependence.
It's not current case. Submodules alternative is **git subtree** and it's my choice.
You can see quick overview [here](https://www.atlassian.com/git/tutorials/git-subtree).
Below you can see quick overview how it's suppose to work with this repo.

### Adding new program
So I don't want to store a few extra megabytes and thousand commits of ten years history of some program in my repo
and I'm not planning (and I can't lol) pushing something into original repo,
while cloning repo I discard everything except last version using flag `--squash`:
```console
~$ git subtree add --prefix dmenu https://git.suckless.org/dmenu master --squash --message="Adding dmenu..."
```
`--prefix` determinate directory name for new program.

### Updating program
Same things with updating program:
```console
~$ git subtree pull --prefix dmenu https://git.suckless.org/dmenu master --squash
```

### Finally, why subtree?
If you want to clone this repo you can do it as usual:
```console
~$ git clone <URL>
```
the all code is available immediately, there is no need to perform additional actions with cloning submodules.

### But wait, I still have extra megabytes from cloning the entire program history!
Just run garbage collection:
```console
~$ git gc --prune=now --aggressive
```
