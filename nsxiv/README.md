# nsxiv
Neo Simple X Image Viewer

## Info about this custom build
### Patches
| Patch                                                                                         | Short description       |
| --------------------------------------------------------------------------------------------- | ----------------------- |
| [color invert](https://codeberg.org/nsxiv/nsxiv-extra/src/branch/master/patches/color-invert) | invert the image colors |

### Additional editing
Not every patch provides updating `usage()` function and documentation, so I manually edit it.
