# dmenu: dynamic menu
dmenu is an efficient dynamic menu for X.

## Info about this custom build
### Patches
| Patch                                                          | Short description                                      |
| -------------------------------------------------------------- | ------------------------------------------------------ |
| [center](https://tools.suckless.org/dmenu/patches/center/)     | centers dmenu in the middle of the screen              |
| [border](https://tools.suckless.org/dmenu/patches/border/)     | adds a border around the dmenu window                  |
| [grid](https://tools.suckless.org/dmenu/patches/grid/)         | render dmenu's entries in a grid                       |
| [gridnav](https://tools.suckless.org/dmenu/patches/gridnav/)   | adds the ability to move left and right through a grid |
| [password](https://tools.suckless.org/dmenu/patches/password/) | display special symbols instead of the keyboard input  |

### Additional editing
Not every patch provides updating `usage()` function and documentation, so I manually edit it.
Also I add setting centered dmenu window width via command line argument `[-W pixels]`.

## Requirements
In order to build dmenu you need the Xlib header files.

## Installation
Edit `config.mk` to match your local setup (dmenu is installed into the `/usr/local` namespace by default).
Afterwards enter the following command to build and install dmenu (if necessary as root):
```console
~$ make clean install
```

## Running dmenu
See the man page for details.
